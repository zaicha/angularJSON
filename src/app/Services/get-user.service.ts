import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Example } from '../Models/Example';
@Injectable({
  providedIn: 'root'
})
export class GetUserService {
private url = "./assets/json/contact.json"
  constructor(private http: HttpClient) { }
  getPhotos(): Observable<Example[]> {
    return this.http.get<Example[]>(this.url);
}
}

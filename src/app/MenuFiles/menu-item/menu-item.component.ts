import { Component, ElementRef, HostListener, Input, OnInit, HostBinding } from '@angular/core';
import { Router } from '@angular/router';

import { MenuItem, MenuService } from '../../services/menu.service';

@Component({
  selector: 'fw-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent implements OnInit {
  @Input() item = <MenuItem>null;  // see angular-cli issue #2034

  // @HostBinding('class.parent-is-popup')
  @Input() parentIsPopup = true;

  isActiveRoute = false;

  mouseInItem = false;
  mouseInPopup = false;
  popupLeft = 0;
  popupTop: number;
  popupcolor = "black";
  constructor(private router: Router, public menuService: MenuService) {
  }

  ngOnInit(): void {

  }

  toggleSubMenu(): void {
    this.item.collapse = !this.item.collapse;
  }


  isActivateRoute(): boolean {
    return this.menuService.checkActivateRoute(this.item.route);
  }
  onPopupMouseEnter(event): void {
    if (!this.menuService.isVertical) {
      this.mouseInPopup = true;
    }
  }

  onPopupMouseLeave(event): void {
    if (!this.menuService.isVertical) {
      this.mouseInPopup = false;
    }
  }

  @HostListener('mouseleave', ['$event'])
  onMouseLeave(event): void {
    this.mouseInItem = false;
  }

  @HostListener('mouseenter')
  // onMouseEnter(): void {
  //   if (!this.menuService.isVertical) {
  //     if (this.item.submenu) {
  //       this.mouseInItem = true;

  //      // this.popupTop = 0;
  //       this.popupLeft=-76;

  //       if (this.parentIsPopup) {
  //         this.popupLeft = 76;
  //         this.popupTop = 2;
  //       }
  //     }
  //   }
  // }
  onMouseEnter(): void {
    //TODO
    // if (!this.menuService.isVertical) {
    //   if (this.item.submenu) {
    //     this.mouseInItem = true;

    //     // this.popupTop = -10;
    //     this.popupLeft = -76;

    //     if (this.parentIsPopup) {
    //       this.popupLeft = 76;
    //       this.popupTop = -30;
    //     }
    //   }
    // }
  }
  @HostListener('click', ['$event'])
  onClick(event): void {
    //event.stopPropagation();
    //pour rendre invisible les composnat popup-menu
    this.mouseInItem = false;
    this.mouseInPopup = false;

    if (this.item.route) {
      this.router.navigate(['/' + this.item.route])

    }
  }
}
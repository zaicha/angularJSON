import { Component, OnInit } from '@angular/core';
import { GetUserService } from './Services/get-user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
   this.userService.getPhotos().subscribe(
     (data)=>{
console.log('user', data);
     }
   );
  }
  title = 'app';

  constructor(private userService:GetUserService){
    
  }
}

import { MenuItem } from '../fw/services/menu.service';
import { DayViewComponent } from './day-view/day-view.component';

export let InitialMenuTems: Array<MenuItem> = [
    {
        text: '',
        icon: "../../assets/icons/home.png", 
        route: 'dashboard',
        collapse:false,
        submenu: null
    },
    {
        text: 'SmartBoard',
        icon:"../../assets/icons/smartboard2.png",  
        route: 'comingsoon',
        collapse:false,
        submenu: [
            {
                text: 'Smart insights',
                // icon: 'fa fa-chevron-circle-right',
                route: null,
                collapse:false,
                submenu:  [
                    {
                        text: 'Smart insights',
                        // icon: 'fa fa-chevron-circle-right',
                        route: null,
                        collapse:false,
                        submenu:null
                    },
                    {
                        text: 'Annual Focus',
                        // icon: 'fa fa-flag',
                        route: null,
                        submenu: null,
                        collapse:false
                    },
                    {
                        text: 'Monthly Focus',
                        // icon: 'fa fa-flag',
                        route: null,
                        submenu: null,
                        collapse:false
                    }
                ],
            },
            {
                text: 'Annual Focus',
                // icon: 'fa fa-flag',
                route: null,
                submenu: null,
                collapse:false
            },
            {
                text: 'Monthly Focus',
                // icon: 'fa fa-flag',
                route: null,
                submenu: null,
                collapse:false
            }
        ],
    },
    {
        text: 'Daily Focus',
      //  icon: 'fa fa-globe',
        icon: "../../assets/icons/dailyfocus.png",
        route: 'comingsoon' ,
        collapse:false,
        submenu: null
    }, 
    {
        text: 'Reporting',
        icon: "../../assets/icons/reporting.png",
        route: 'comingsoon',
        collapse:false,
        submenu: null
    },
    {
        text: 'Tutoriel',
        icon: "../../assets/icons/video.png",
        route: 'gallery',
        collapse:false,
        submenu: null
    }
];